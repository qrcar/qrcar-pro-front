import security from '@/apis/security'

export default (axios) => ({
  security: security(axios),
})
