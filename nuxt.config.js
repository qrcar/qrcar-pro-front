export default {
  mode: 'spa',
  /*
   ** Headers of the page
   */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: ['@/assets/scss/app.scss', '@fortawesome/fontawesome-svg-core/styles.css'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/fontawesome.js',
    '~/plugins/globalDirectives.js',
    '@/plugins/api.js',
    // '~/plugins/validate.js'
    // '~/plugins/globalComponents.js',
    { src: '~/plugins/vuex-persist', mode: 'client' },
    { src: '~plugins/v-calendar.js', ssr: false },
  ],
  styleResources: {
    scss: '@/assets/scss/app.scss',
  },
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [],
  /*
   ** Nuxt.js modules
   */
  modules: [
    'bootstrap-vue/nuxt',
    //['bootstrap-vue/nuxt', { css: false }],
    '@nuxtjs/auth',
    '@nuxtjs/axios',
    '@nuxtjs/style-resources',
    [
      'bootstrap-vue/nuxt',
      {
        bootstrapCSS: false,
        bootstrapVueCSS: false,
        componentPlugins: ['Carousel', 'Spinner'],
        directivePlugins: ['Tooltip', 'Popover'],
      },
    ],
  ],
  module: {
    rules: [
      {
        test: /\.s[ac]ss$/i,
        use: ['style-loader', 'scss-loader', 'sass-loader'],
      },
    ],
  },
  axios: {
    baseURL: 'http://localhost:8000',
  },
  router: {},
  /*
   ** Build configuration
   */
  build: {
    loaders: {
      scss: { sourceMap: false },
    },
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Exécuter ESLint lors de la sauvegarde
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        })
      }
    },
  },
}
